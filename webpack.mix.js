let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/BibleVerses.js', 'src/assetbundles/bibleversesfield/dist/js')
    .sass('resources/sass/bibleverses.scss', 'src/assetbundles/bibleversesfield/dist/css');

if (mix.inProduction()) {
    mix.version();
}