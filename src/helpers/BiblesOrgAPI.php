<?php
namespace tippingmedia\verses\helpers;

use tippingmedia\verses\Verses;

use Craft;
use craft\helpers\FileHelper;

/**
 * Location helper class.
 *
 * @author    Tipping Media LLC. <support@tippingmedia.com>
 * @copyright Copyright (c) 2016, Tipping Media LLC.
 * @see       http://tippingmedia.com
 * @package   location.helpers
 * @since     2.0
 */

class BiblesOrgAPI
{
    public static function baseURI():string
    {
        return "https://bibles.org";
    }

    public static function apiURL():string
    {
        return "/v2/";
    }

    public static function queryParams($options = []): string
    {

        $settings = Craft::$app->getPlugins()->getPlugin('verses')->getSettings();
        $api = $options['apiType'];

        $version = array_key_exists('version',$options) ? $options['version'] : $settings['bibleVersion'];
        $marginalia = array_key_exists('marginalia',$options) ? $options['marginalia'] : false;

        unset($options['apiType']);
        $params = http_build_query(array_filter($options));
        $query;
        

        switch ($api) 
        {

            case 'passage':
            
            if (array_key_exists('passage',$options)) 
            {
                $query = "passages.js" . "?q[]=" . strtolower($options['passage']) . "&version=" . $version . "&include_marginalia=" . $marginalia;
            }
            else
            {
                $query = "passages.js" . "?q[]=" . strtolower($options['book']) . "+" . $options['verse'] . "&version=" . $version . "&include_marginalia=" . $marginalia;
            }
            break;

            case 'chapters':
                // osis eng-ESV:2Tim
                // endpoint ==
                if (array_key_exists('endpoint',$options)) 
                {
                    if ($options['endpoint'] == 'books') 
                    {
                        $query = "books/" . $version .':'. $options['osis'] . ".js?" . "include_marginalia=" . $marginalia;
                    }
                    elseif($options['endpoint'] == 'chapters')
                    {
                        $query = "chapters/" . $version .':'. $options['osis'] . ".js?" . "include_marginalia=" . $marginalia;
                    }
                }
                else
                {
                    $query = "chapters/" . $version .':'. $options['osis'] . ".js?" . "include_marginalia=" . $marginalia;
                }
                
            break;

            case 'search':
                $query = "search.js?" . $params;
            break;

            case 'books':
                // &include_chapters = true
                // &testament = NT
                
                $query = "versions/" . $options['version'] . "/books.js?include_chapters=true";
            break;

            case 'verses':
                if (array_key_exists('start', $options) && array_key_exists('end', $options)) 
                {
                $query = "chapters/" . $version .':'. $options['chapter'] . "/verses.js?start=" . $options['start'] . "&end=" . $options['end'];
                }
                else
                {
                    $query = "chapters/" . $version .':'. $options['chapter'] . "/verses.js";
                }
                
            break;
            
            default:
                return false;

            break;
        }

        return $query;
    }
}
