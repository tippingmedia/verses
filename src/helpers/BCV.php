<?php
namespace tippingmedia\verses\helpers;

use tippingmedia\verses\Verses;

use Craft;
use craft\helpers\FileHelper;

/**
 * Verses helper class.
 *
 * @author    Tipping Media LLC. <support@tippingmedia.com>
 * @copyright Copyright (c) 2016, Tipping Media LLC.
 * @see       http://tippingmedia.com
 * @package   location.helpers
 * @since     2.0
 */

class BCV
{
    // return books & chapters json array
    public static function bc() 
    {
        $string = file_get_contents(__DIR__ . "/bcv.json");
        $json_a = json_decode($string, true);
        return $json_a;
        
    }
}
