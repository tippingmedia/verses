<?php
namespace tippingmedia\verses\helpers;

use tippingmedia\verses\Verses;

use Craft;
use craft\helpers\FileHelper;

/**
 * Location helper class.
 *
 * @author    Tipping Media LLC. <support@tippingmedia.com>
 * @copyright Copyright (c) 2016, Tipping Media LLC.
 * @see       http://tippingmedia.com
 * @package   location.helpers
 * @since     2.0
 */

class ESVAPI
{
    public static function baseURI():string
    {
        return "https://api.esv.org";
    }

    public static function apiURL():string
    {
        return "/v3/passage/html/";
    }

    public static function fums():string
    {
        return '<p class="copyright">The Holy Bible, English Standard Version copyright ©&nbsp;2001 by <a href="http://www.crosswaybibles.org">Crossway</a>, a publishing ministry of Good News Publishers. Used by permission. All rights reserved. <a href="http://www.crossway.org/page/esv.copyright">Quotation information</a>.</p>';
    }

    public static function queryParams($options = []): array
    {
        $defaults = [];
        $options['q'] = $options['passage'];

        $defaults['include-audio-link'] = 'false';
        $defaults['include-crossrefs'] = 'false';
        $defaults['include-footnotes'] = 'false';
        $defaults['include-copyright'] = 'true';
        $defaults['include-short-copyright'] = 'false';
        $defaults['include-passage-references'] = 'true';
        $defaults['include-chapter-numbers'] = 'true';
        $defaults['include-headings'] = 'true';
        $defaults['include-subheadings'] = 'true';

        // Add defaults if not in options
        foreach ($defaults as $keys => $values) {
            if (! array_key_exists($keys, $options)) {
                $options[$keys] = $values;
            }
        }

        unset(
            $options['apiType'], 
            $options['version'], 
            $options['marginalia'],
            $options['endpoint'],
            $options['passage'],
            $options['osis']);

        //$result = http_build_query(array_filter($options));

        return $options; //str_replace('&', ',', $result);
    }
}
