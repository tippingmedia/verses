<?php
/**
 * Verses plugin for Craft CMS 3.x
 *
 * Craft CMS Plugin for finding and outputting bible verses.
 *
 * @link      https://tippingmedia.com
 * @copyright Copyright (c) 2018 Tipping Media LLC
 */

namespace tippingmedia\verses\services;

use tippingmedia\verses\Verses;
use tippingmedia\verses\helpers\ESVAPI;
use tippingmedia\verses\helpers\BiblesOrgAPI as BiblesAPI;

use Craft;
use craft\base\Component;

/**
 * Guzzle Service
 *
 * All of your plugin’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Tipping Media LLC
 * @package   Verses
 * @since     2.0.0
 */
class Guzzle extends Component
{
    //protected $baseUrl = 'https://bibles.org/v2/';
    protected $fums = '';
    protected $copyright = '';

    // Public Methods
    // =========================================================================

    /**
     * Get
     * Verses::$plugin->guzzle->get($options)
     */

    public function get($options)
    {

        // Plugin settings
        $settings = Craft::$app->getPlugins()->getPlugin('verses')->getSettings();

        // API Token
        $token = $settings->token; 

        //Bible version defaults to settings (eng-NASB)
        $version = array_key_exists('version', $options) ? $options['version'] : $settings->bibleVersion;
        //\yii\helpers\VarDumper::dump($options, 5, true);exit;

        if($version == "eng-ESV") {
            $uri = ESVAPI::baseURI();
            $path = ESVAPI::apiURL();
            $query = ESVAPI::queryParams($options);
            $osis = $options['osis'];
        } else {
            // Get bibles.org api specific url params
            $uri = BiblesAPI::baseURI();
            $path = BiblesAPI::apiURL() . BiblesAPI::queryParams($options);
            $osis = array_key_exists('osis', $options) ? $options['osis'] : '';
            $query = [];
        }

        // Check to see if the response is cached
        $cachedResponse = false; //Craft::$app->fileCache->get($url);

        if ($cachedResponse) {
            // adds to the FUMS array
            $this->fums = $cachedResponse['response']['meta']['fums'];
            return $cachedResponse;
        }

        $client = new \GuzzleHttp\Client(['base_uri' => $uri]);
        //\yii\helpers\VarDumper::dump($uri . $path, 5, true);exit;
        try 
        {
           
            $response = $client->request('GET', $path, $this->guzzleOptions(['version' => $version, 'token' => $token, 'query' => $osis, 'params' => $query]));

            $body = json_decode($response->getBody(), true);


            //set FUMS Array
            if($version != 'eng-ESV') {
                $this->fums = $body['response']['meta']['fums'];
                //\yii\helpers\VarDumper::dump($this->recursive_array_search('copyright', $body['response']), 5, true);exit;
            }
            
            return [
                'statusCode' => $response->getStatusCode(),
                'reason' => $response->getReasonPhrase(),
                'body' => $body
            ];
            // Cache the response
            //Craft::$app->fileCache->set($url, $references);
        } 
        catch(\Exception $e) 
        {
            return [
                'error' => true,
                'reason' => $e->getMessage()
            ];
        }
    }


    public function guzzleOptions($params) {
        // ESV api needs different authorization than Bibles.org
        if($params['version'] == "eng-ESV") {
            return [
                'query' => $params['params'],
                'allow_redirects' => true,
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => "Token {$params['token']}"
                ]
            ];
        } else {
            return [
                'allow_redirects' => true,
                'auth' => [
                    "{$params['token']}",
                    "X"
                ]
                // 'curl' => [
                //     CURLOPT_SSL_VERIFYHOST => 0,
                //     CURLOPT_SSL_VERIFYPEER => 0,
                //     CURLOPT_RETURNTRANSFER => 1,
                //     CURLOPT_FOLLOWLOCATION => 1,
                //     CURLOPT_HTTPAUTH => 'CURLAUTH_BASIC',
                //     CURLOPT_USERPWD => $params['token'].':X'
                // ]
            ];
        }
    }


    /**
     * Returns an array of Fair Usage Management Scripts 
     * required by Bibles.org
     * @return array js script text
     */
    public function fums()
    {
        return $this->fums;
    }

    public function setCopyright($copyright) {
        $this->copyright = $copyright;
    }

    /**
     * Returns copyright of current used bible version
     * required by Bibles.org
     * @return array js script text
     */
    public function copyright()
    {
        return $this->copyright;
    }

}
