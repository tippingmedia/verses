<?php
/**
 * Verses plugin for Craft CMS 3.x
 *
 * Craft CMS Plugin for finding and outputting bible verses.
 *
 * @link      https://tippingmedia.com
 * @copyright Copyright (c) 2018 Tipping Media LLC
 */

namespace tippingmedia\verses\services;

use tippingmedia\verses\Verses;

use Craft;
use craft\base\Component;

/**
 * Fums Service
 *
 * All of your plugin’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Tipping Media LLC
 * @package   Verses
 * @since     2.0.0
 */
class Fums extends Component
{
    // Public Methods
    // =========================================================================

}
