<?php
/**
 * Verses plugin for Craft CMS 3.x
 *
 * Craft CMS Plugin for finding and outputting bible verses.
 *
 * @link      https://tippingmedia.com
 * @copyright Copyright (c) 2018 Tipping Media LLC
 */

namespace tippingmedia\verses\variables;

use tippingmedia\verses\Verses;
use tippingmedia\verses\helpers\BCV;
use tippingmedia\verses\helpers\ESVAPI;

use Craft;
use craft\helpers\Template;

/**
 * Verses Variable
 *
 * Craft allows plugins to provide their own template variables, accessible from
 * the {{ craft }} global variable (e.g. {{ craft.verses }}).
 *
 * https://craftcms.com/docs/plugins/variables
 *
 * @author    Tipping Media LLC
 * @package   Verses
 * @since     2.0.0
 */
class VersesVariable
{
    // Public Methods
    // =========================================================================

    /**
     * Return books of the bible
     *
     * @param  array $criteria [testament = NT|OT , include_chapters = bool]
     * @return array of bible books
     */
    public function books($criteria = [])
    {
        return BCV::bc();
    }


    /**
     * Return a passage of scripture
     * @param  array  $criteria [(passage || book,verse),version,marginalia]
     * @return array  http://bibles.org/pages/api/documentation/books
     */
    public function passage($criteria = [])
    {
        // Plugin settings
        $settings = Craft::$app->getPlugins()->getPlugin('verses')->getSettings();

        //\yii\helpers\VarDumper::dump($response, 5, true);exit;
        if($settings['bibleVersion'] == "eng-ESV") {
            $response = Verses::$plugin->guzzle->get($criteria);
            return [
                'text' => Template::raw($response['body']['passages'][0]),
                'reference' => $response['body']['canonical']
            ];
        } else {
            $criteria['apiType'] = 'passage';
            $criteria['passage'] = str_replace(" ", "+", $criteria['passage']);
            $response = Verses::$plugin->guzzle->get($criteria);
            Verses::$plugin->guzzle->setCopyright($response['body']['response']['search']['result']['passages'][0]['copyright']);
            return [
                'text' => Template::raw($response['body']['response']['search']['result']['passages'][0]['text']),
                'reference' => Template::raw($response['body']['response']['search']['result']['passages'][0]['display'])
            ];
        }
         //$response['response']['search']['result']['passages'];
    }


    /**
     * Return bible verses
     * @param  array  $criteria [chapter:OSIS,apiType,start,end] if start must have end
     * @return array  verses
     */
    public function verses($criteria = [])
    {
        $criteria['apiType'] = 'verses';
        $response = Verses::$plugin->guzzle->get($criteria);
        //\yii\helpers\VarDumper::dump($response, 5, true);exit;
        Verses::$plugin->guzzle->setCopyright($response['body']['response']['verses'][0]['copyright']);
        return $response['body']['response']['verses'];
    }


    public function fums()
    {
        $settings = Craft::$app->getPlugins()->getPlugin('verses')->getSettings();
        if($settings['bibleVersion'] == "eng-ESV") {
            $response = ESVAPI::fums();
        } else {
            $response = Verses::$plugin->guzzle->fums();
        }
        
        return $response;
    }

    public function copyright() {
        $settings = Craft::$app->getPlugins()->getPlugin('verses')->getSettings();
        if($settings['bibleVersion'] == "eng-ESV") {
            $response = ESVAPI::fums();
        } else {
            $response = Verses::$plugin->guzzle->copyright();
        }
        
        return $response;
    }
}
