/**
 * Verses plugin for Craft CMS
 *
 * Verses JS
 *
 * @author    Tipping Media LLC
 * @copyright Copyright (c) 2018 Tipping Media LLC
 * @link      https://tippingmedia.com
 * @package   Verses
 * @since     2.0.0
 */
