<?php
/**
 * Verses plugin for Craft CMS 3.x
 *
 * Craft CMS Plugin for finding and outputting bible verses.
 *
 * @link      https://tippingmedia.com
 * @copyright Copyright (c) 2018 Tipping Media LLC
 */

namespace tippingmedia\verses\controllers;

use tippingmedia\verses\Verses;

use Craft;
use craft\web\Controller;

/**
 * Verses Controller
 *
 * Generally speaking, controllers are the middlemen between the front end of
 * the CP/website and your plugin’s services. They contain action methods which
 * handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering
 * post data, saving it on a model, passing the model off to a service, and then
 * responding to the request appropriately depending on the service method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what
 * the method does (for example, actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 *
 * @author    Tipping Media LLC
 * @package   Verses
 * @since     2.0.0
 */
class VersesController extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = ['actionGetPassages', 'actionGetFums'];

    // Public Methods
    // =========================================================================

   /**
     * Get passages of bible
     * @return array array objects from bibles.org api
     */
    public function actionGetPassages()
    {
        $request = Craft::$app->getRequest();
        if ($request->getAcceptsJson()) {
            $postData = $request->getBodyParams();
            $verses = Verses::$plugin->guzzle->get($postData);
            return $this->asJson($verses);
        }
    }
    

    /**
     * return Fair Use Management scripts from Bibles.org api.
     * @return JSON array of scripts
     */
    public function actionGetFums()
    {
        $request = Craft::$app->getRequest();
        if ($request->getAcceptsJson()) {
            $fums = Verses::$plugin->guzzle()->fums();
            return $this->asJson($fums);
        }
    }
}
