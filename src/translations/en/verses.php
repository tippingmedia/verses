<?php
/**
 * Verses plugin for Craft CMS 3.x
 *
 * Craft CMS Plugin for finding and outputting bible verses.
 *
 * @link      https://tippingmedia.com
 * @copyright Copyright (c) 2018 Tipping Media LLC
 */

/**
 * Verses en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('verses', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Tipping Media LLC
 * @package   Verses
 * @since     2.0.0
 */
return [
    'Verses plugin loaded' => 'Verses plugin loaded',
];
