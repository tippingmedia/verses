# Verses
#### Craft CMS 3 Plugin for finding and outputting bible verses.


### Installation 
Download the folder extract and place the **Verses** folder in *Craft > Plugins* folder. In the Craft CMS backend go to *Settings > Plugins* an click to install the Verses Plugin. 

Obtain an API key from [http://bibles.org/](http://bibles.org/pages/api) and add this key to the settings of the plugin. Select a default version of the bible.

If you want to use the English Standard Version (ESV) you can obtain an API key from [https://api.esv.org/](https://api.esv.org/)

Add the Verses input to one of your Sections.

### Usage
*To comply with Bibles.org Fair Use Management Rules this code must be included in your template you are using verses plugin to output passages. 

```
{% set fums = craft.verses.fums() %}
{{ fums|raw }}
```
### Copyright
*Make sure to output the copyright with your passages, verses or chapters. 

```
{% set copyright = craft.verses.copyright() %}
{{ copyright|raw }}
```

### Verses Input
The input saves an array of objects. The single object includes:

```
{
    reference: Romans 10:9,
    osis: Rom.10.9
}
```

#### To output your first reference from the input.

```
{{ entry.versesInput[0].reference }}
{{ entry.versesInput[0].osis }}
```

You can specify a different version than the one selected in the Verses settings.

```
{% for item in entry.versesInput %}
    {{ item.reference }}
    {{ item|passage({'version':'eng-ESV'})}}
{% endfor %}
```

#### Output a chapter without the input
*note it uses OSIS notation & currently only available for Bibles.org API.

```
{% set passages = craft.verses.verses({'chapter':'Rom.8'}) %}
{% if passages|length %}
    {% for item in passages %}
        {{ item.text|raw }}
        {% if loop.last %}
            <p class="copyright">{{ item.copyright|raw }}</p>
        {% endif %}
    {% endfor %}
{% endif %}
```

#### Output the books of the bible
```
{% set books = craft.verses.books() %}
{% for item in books %}
    {{ item.name }}
{% endfor %}
```


### License
This plugin relies on Bibles.org API or Crossways ESV API.  For Bibles.org you will need to follow their [Fair Use Management Rules](http://bibles.org/pages/api#what-is-fums) to use this plugin. 
Use this plugin at your own risk. We are not responsible for any damage to your content, code, or anything else due to the use of this plugin. It is meant to be a blessing, and take it as such.


### Credit
Many thanks to [Bibles.org](http://bibles.org) for providing api to search the word of God. Second I would like to thank [Openbible.info](https://github.com/openbibleinfo) for the Bible Passage Reference Parser. 