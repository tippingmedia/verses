import Vue from 'vue';
import MultiVue from 'vue-multivue';
import VModal from 'vue-js-modal';
import Verses from './components/Verses.vue';

window.Vue = Vue;
window.Event = new Vue();
Vue.use(VModal);

new MultiVue('.bv-field', {
    components: {
        Verses
    }
});